a_string = "the sting\n split and mangled\t\t in the bank account example "
print(a_string)

b_string = "the string" + chr(13) + "split and mangled" + chr(10) + "in the  account example"
print(b_string)

c_string = r"the strings are\t splitted and \narranged"  # raw lateral string
print(c_string)

d_string = "the string split and mangled in the \followed by bank account example "  # escape character
print(d_string)

d1_string = "the string split and mangled in the \\followed by bank account example "
print(d1_string)

d2_string = "the string is enclosed \\"
print(d2_string)

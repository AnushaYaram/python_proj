letters = "abcdefghijklmnopqrstuvwxyz"
backwards1 = letters[-10:-13:-1]  # [16:13:-1]
print(backwards1)
backwards2 = letters[-22::-1]  # [4::-1]
print(backwards2)
backwards3 = letters[:-9:-1]
print(backwards3)
print(letters[-4:])
print(letters[-1:])
print(letters[:1])
print(letters[0])

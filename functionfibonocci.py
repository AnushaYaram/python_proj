# def fib(n):
#     if n < 2:
#         return n
#     else:
#         return fib(n - 1) + fib(n - 2)

def fibonocci(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        n_minus1 = 1
        n_minus2 = 0
        for f in range(1, n + 1):
            result = n_minus2 + n_minus1
            n_minus2 = n_minus1
            n_minus1 = result
    return result


for i in range(10):
    print(i, fibonocci(i))

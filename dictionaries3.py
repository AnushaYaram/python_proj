fruits = {"apple": "good for health,good for making cider",
          "lemon": "sour,contains citrus more in this fruit",
          "grape": "seems small,it grows like bunches",
          "lime": "a sour fruit,green citrus fruit"}
# for i in range(10):
#     for snacks in fruits:
#         print(snacks + " is "+fruits[snacks])
#     print("-"*40)

print(fruits)
# ordered_keys = list(fruits.keys())
# ordered_keys.sort()
# ordered_keys = sorted(list(fruits.keys()))
# for f in sorted(fruits.keys()):
#     print(f + "-" + fruits[f])
# for val in fruits.values():
#     print(val)
# fruits_keys = fruits.keys()
# print(fruits_keys)

print(fruits.items())
f_tuple = tuple(fruits.items())
print(f_tuple)
print(dict(f_tuple))


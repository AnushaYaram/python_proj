fruits = {"apple": "good for health,good for making cider",
          "lemon": "sour,contains citrus more in this fruit",
          "grape": "seems small,it grows like bunches",
          "lime": "a sour fruit,green citrus fruit"}
print(fruits)

print(fruits["apple"])
print(fruits["lemon"])
print(fruits["grape"])
print(fruits["lime"])
# fruits["pear"] = "an old shaped apple"
# print(fruits)
# fruits["grape"] = "black in colour"
# print(fruits)
# del fruits["grape"]
# print(fruits)
# fruits.clear()
# print(fruits)
print()

numbers = {1: "one,first number",
           2: "two,second number",
           3: "three,third number",
           4: "four,fourth number",
           5: "five,fifth number"}
print(numbers)
print(numbers[1])
print(numbers[2])
print(numbers[3])
print(numbers[4])
print(numbers[5])
print()

items = {1: "one", "name": "anu,sweet", 3: "three", "name1": "ash"}
print(items)
print(items[1])
print(items["name"])
print(items["name1"])
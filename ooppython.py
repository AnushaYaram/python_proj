class Kettle(object):
    power_source = "electricity"

    def __init__(self, make, price):
        self.make = make
        self.price = price
        self.on = False

    def switch_on(self):
        self.on = True


kenwood = Kettle("kenwood", 5.65)
print(kenwood.price)
print(kenwood.make)

hamilton = Kettle("hamilton", 8.87)
print(hamilton.price)
print(hamilton.make)
hamilton.price = 98
print(hamilton.price)

print("models:{} {} ={} {}".format(kenwood.make, kenwood.price, hamilton.make, hamilton.price))

print(hamilton.on)
hamilton.switch_on()
print(hamilton.on)

Kettle.switch_on(kenwood)
print(kenwood.on)
kenwood.switch_on()

print(Kettle.power_source)
print(kenwood.power_source)
print(hamilton.power_source)
print(Kettle.__dict__)
print(kenwood.__dict__)
print(hamilton.__dict__)
print("switch to atomic power")
Kettle.power_source = "atomic"
print(kenwood.power_source)
print("switch kenwood to gas")
kenwood.power_source = "gas"
print(kenwood.power_source)

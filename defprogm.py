def even_odd(num) -> object:
    if num % 2:
        return 'odd number'
    else:
        return 'even number'


result1 = even_odd(7)
print(result1)

result2 = even_odd(4)
print(result2)

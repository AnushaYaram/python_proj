import datetime
import pytz


class AccountNumber:
    def __init__(self, name, balance):
        self.name = name
        self.balance = balance
        self.transaction_list = []
        print("Account is created for " + self.name)

    def deposit(self, amount):
        if amount > 0:
            self.balance += amount
            self.show_balance()
            self.transaction_list.append((pytz.utc.localize(datetime.datetime.utcnow()), amount))

    def withdraw(self, amount):
        if 0 < amount <= self.balance:
            self.balance -= amount
        else:
            print("the amount must be greater than zero and no more than your account balance")
            self.show_balance()

    def show_balance(self):
        print("the balance is {} ".format(self.balance))

    def show_transactions(self):
        for date, amount in self.transaction_list:
            if amount > 0:
                tran_type = "deposited"
            else:
                tran_type = "withdrawn"
                amount *= -1
            print("{:6} {} on {} (local time was {})".format(amount, tran_type, date, date.astimezone()))


if __name__ == '__main__':
    anu = AccountNumber("anusha", 0)
    anu.show_balance()
    anu.deposit(1000)
    anu.withdraw(200)
    anu.show_balance()
    anu.withdraw(5000)
    anu.show_transactions()

parrot = "Norwegian Blue"  # 012345678910111213
print(parrot)
print(parrot[3])  # positive indexing
print(parrot[4])
print(parrot[9])
print(parrot[3])
print(parrot[6])
print(parrot[8])

print(parrot[-11])  # negative indexing
print(parrot[-10])
print(parrot[-5])
print(parrot[-11])
print(parrot[-8])
print(parrot[-6])
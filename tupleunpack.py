x, y, z = 3, 4, 5
print(x)
print(y)
print(z)


print("unpacking the tuple")
data_list = 6, 7, 8, 9

x, y, z, r = data_list
print(x)
print(y)
print(z)
print(r)

# print("unpacking the list")
#
# data = [15,16,17]
# data.append(18)     #we can't append the value because p,q,r has 3 variables not 4 to append
# p, q, r = data
# print(p)
# print(q)
# print(r)

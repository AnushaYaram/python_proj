import datetime
import pytz


class AccountNumber:
    @staticmethod
    def _current_time():
        utc = datetime.datetime.utcnow()
        return pytz.utc.localize(utc)

    def __init__(self, name, balance):
        self._name = name
        self.__balance = balance
        self._transcation_list = [(AccountNumber._current_time(), balance)]
        print("Account is created for " + self._name)
        self.show_balance()

    def deposit(self, amount):
        if amount > 0:
            self.__balance += amount
            self.show_balance()
            self._transcation_list.append((AccountNumber._current_time(), amount))

    def withdraw(self, amount):
        if 0 < amount <= self.__balance:
            self.__balance -= amount
            self._transcation_list.append((AccountNumber._current_time(), -amount))
        else:
            print("the amount must be greater than zero and no more than your account balance")
            self.show_balance()

    def show_balance(self):
        print("the balance is {} ".format(self.__balance))

    def show_transactions(self):
        for date, amount in self._transcation_list:
            if amount > 0:
                tran_type = "deposited"
            else:
                tran_type = "withdrawn"
                amount *= -1
            print("{:6} {} on {} (local time was {})".format(amount, tran_type, date, date.astimezone()))


if __name__ == '__main__':
    anu = AccountNumber("anusha", 0)
    anu.show_balance()
    anu.deposit(1000)
    anu.withdraw(200)
    anu.show_balance()
    anu.withdraw(5000)
    anu.show_transactions()

    chandu = AccountNumber("chandu", 500)
    chandu.__balance = 200
    chandu.deposit(300)
    chandu.withdraw(100)
    chandu.show_transactions()
    chandu.show_balance()

even = set(range(0, 20, 2))
print(sorted(even))
squares_tuple = (9, 16, 4, 36, 25)
squares = set(squares_tuple)
print(sorted(squares))

print("even minus squares")
print(sorted(even.difference(squares)))
print(sorted(even - squares))

print("squares minus even")
print(squares.difference(even))
print(squares - even)
print("=" * 40)


print("symmetric even minus squares")
print(sorted(even.symmetric_difference(squares)))

print("symmetric squares minus even")
print(squares.symmetric_difference(even))

squares.discard(25)
squares.remove(9)
squares.discard(16)
print(squares)

if squares.issubset(even):
    print("squares is a subset of even")

if even.issuperset(squares):
    print("even is a superset of squares")


even = frozenset(range(0, 100, 2))

print(even)

